import requests
import json
import urllib
import contextlib

headers = {
    'Authorization': 'Token  acebf2347a13ba29d77b1be4b39a0201630625ae',
}


response = requests.get('https://10.1.0.91/api/virtualization/virtual-machines/' , headers=headers, verify=False)
all_results = json.loads(response.text)['results']
next_url = json.loads(response.text)['next']
while next_url:
    print(next_url)
    response = requests.get(next_url, headers=headers, verify=False)
    all_results += json.loads(response.text)['results']
    next_url = json.loads(response.text)['next']


 
file_path=open("netbox_returned_data.json","w")
json.dump(all_results,file_path,indent=6)
file_path.close()