configuration = [
{% for i in infos -%}   
    
    { "container_name" : "{{i.name}}",
     "no_of_instances" : "1" ,
     "cpu" : "{{i.vcpus}}",
     "memory" : "{{i.memory}}"
    },

    
    
{% endfor %}
]