terraform {
  required_providers {
    lxd = {
      source = "terraform-lxd/lxd"
    }
  }
}

provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true

  lxd_remote {
    name     = "sv01"
    scheme   = "https"
    address  = "149.202.84.161"
    password = "nanoDC"
    default  = true
  }
}


resource "lxd_profile" "lxdprofile1" {
       name = "lxdprofile1"
       config = {
            
             "boot.autostart" = true
             "user.user-data": <<EOT
               #cloud-config
               users:
               - default
               - name: oumaima
                 gecos: oumaima
                 groups: sudo
                 sudo: ALL=(ALL) NOPASSWD:ALL
                 shell: /bin/bash
                 lock_passwd: false
                 ssh-authorized_keys: 
                 - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQClu4FbvN68oZX3iRqV+6XLgO78/BgVXdgrUXiR5AU8Z2UlxjjEpGSt4QvMWWlOlMhU2eAFPxQRPfTsVJc+KlNmU2cOumSUCk/5BBeB0W9km0tJCqsHoJBwIJEeBXtT+bl3w5T0JSGdITvRpHGMrdUG3WyPBIlG63ZQpTSdvjmrNxLPYi3TJoOyPbOoYT6arSIuQp2b4zPGq1hJBOz++RCZyjsYi2aDIDv7FYqpw3M3Gd0BFfUcTireqn8LSyO4m+rYKIKYEVNdHPThQ9IIEnjB6UIgKIASGRTRGzrO6JCzH9bkjSzZwM4VYlfR3v1J+bvVJTvDXAaSaTE67MgxQbUxceRJA1m1f89akdQ/b2d5pmTcS07rQXlXbhHFWp/Kg6oyYzBFkhqxNC0eo1jGa4kDyf5Y8pdfC+9ujSmrodz7Mt04qCr0jquO0jVRAItIc4G6K+phCQIzIzypvpFQXf4lVZXFK6qRi73Rj6qTRlbvjnuA61Ep3l9twDMUwVU3GEM= oumaima@ubuntu
               package_update: true
               package_upgrade: true
               packages:
               - pip
               - openssh-server
               - nano
               - openjdk-8-jdk
               - curl
              EOT
            
  } 

                                                                                                                                                                        
                device {
                  name = "eth0"
                  type = "nic"
                  properties = {
                          "name" = "eth0"
                          "nictype" = "bridged"
                          "parent" = "lxdbr0"
                               }
                       }
                device {
                  name = "root"
                  type = "disk"
                  properties = {
                             "pool" = "lxd-pool"
                             "path" = "/"
                                }
                       }

  
}




locals {
  serverconfig = [
     for srv in var.configuration : [
        for i in range(1, srv.no_of_instances+1) : { 
            instance_name = "${srv.container_name}-${i}"
            no_cpu = "${srv.cpu}"
            memory_limits = "${srv.memory}"
            }
            ]]
            }
            


locals {
   instances = flatten(local.serverconfig)
   ssh_user         = "oumaima"
   private_key_path = "keys/id_rsa"
   }




resource "lxd_container"  "container" {
  for_each = { for server in local.instances: server.instance_name => server }
           name = "${each.value.instance_name}"
           image = "ubuntu:20.04"
           ephemeral = "false"
           profiles = ["lxdprofile1"]
           limits = {
               cpu = "${each.value.no_cpu}"
               memory = "${each.value.memory_limits}MB"
                    }


  provisioner "remote-exec" {
      inline = ["echo 'Wait until SSH is ready'"]
  }
    
    connection {
    type        = "ssh"
    user        = "oumaima"
    private_key = file(local.private_key_path)
    host        = self.ip_address
    }
    

    provisioner "local-exec" {
      command = "ansible-playbook  -i ${self.ip_address}, --private-key ${local.private_key_path} playbook1.yml"
  }
  

} 

#resource "lxd_snapshot" "snapshot" {
 #  for_each = { for server in local.instances: server.instance_name => server }
  #      container_name = "${each.value.instance_name}"
   #     name           = "${each.value.instance_name}-${"snap"}"
#}
























