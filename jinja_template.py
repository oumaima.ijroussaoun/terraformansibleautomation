from jinja2 import Template
from jinja2 import Environment,FileSystemLoader
import json
import string

file=open('netbox_returned_data.json',"r")
f=json.load(file)

# Dictionary Count in List
# Using list comprehension + isinstance()
res = len([ele for ele in f if isinstance(ele, dict)])


cpu=list()
mem=list()
names=list()
for i in range(0,res):
  var=dict(f[i])
  #print("vcpus:"+ str(var['vcpus']) + " memory:" + str(var['memory']))
  cpu.append(int(var['vcpus']))
  mem.append(var['memory'])   
  names.append((var['name'].replace(" " ,"")))  

l=list()
for c, m, n in zip(cpu, mem, names):
  #l.append({c: m})
  l.append({'vcpus':c,'memory':m,'name':n})



file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)
template = env.get_template('for_template.txt')
output = template.render(infos=l)
#print(output)


import contextlib
 
file_path = 'dev.tfvars'
with open(file_path, "w") as o:
   with contextlib.redirect_stdout(o):
        print(output)




